//  - Express.js - Data persistence via Mongoose ODM
// ODM - Object Document Mapper
/* 
    Tool that translates objects in code to documents for use in document-based databases such as MongoDB
*/

// What is Mongoose?
/* 
    An ODM library that manages data relationships, validates schemas, and simplifies MongoDB document manipulation via the use of models.
*/

// Schemas
/* 
    is a representation of a document's structure. It also contains a document's expected properties and data types.
*/

// Models
/* 
    A programming interface for querying or manipulationg a database.
*/


// Session 35 Activity Instructions:
/* 
    1. Create a user schema.
    2. Create a user model.
    3. Create a POST route that will access the /signup route that will create a user.
*/

// Registering a user
// Business Logic
/* 
    1. Add a functionality to check if there are duplicate tasks
        - If the user already exists in the database, we return an error
        - If the user doesn't exist in the database, we add it in the database
    2. The user data will be ccoming from the request's body
    3. Create a new User object with a "username" and "password" fields/properties
*/

/* 
    4. Process a POST request at the /signup route using postman to register a user.
    5. Create a git repository named s35.
    6. Initialize a local git repository, add the remote link and push to git with the commit message of "Add s35 activity code."
    7. Add the link in Boodle.
    8. Send a screenshot of the /signup route in Postman to our Batch Hangouts

    Stretch Goal:
    Create a route that will retrieve all users.
*/

// Activity solution
// Registering a user
// Business Logic
/*
1. Add a functionality to check if there are duplicate tasks
	- If the user already exists in the database, we return an error
	- If the user doesn't exist in the database, we add it in the database
2. The user data will be coming from the request's body
3. Create a new User object with a "username" and "password" fields/properties
*/

const userSchema = new mongoose.Schema({
	username : String,
	password: String
})

const User = mongoose.model("User", userSchema);

//Register a user

app.post("/signup",(req,res)=>{
	User.findOne({username: req.body.username},(err,result)=>{
		if(result != null && result.username == req.body.username){
			return res.send("Duplicate username found");
		}else{
			if(req.body.username !== '' && req.body.password !== ''){
				
				let newUser = new User({
					username : req.body.username,
					password : req.body.password
				});

				newUser.save((saveErr,savedTask)=>{
					if(saveErr){
						return console.log(saveErr);
					}else{
						return res.status(201).send("New user registered")
					}
				});
			}else{
				return res.send("Both username and password must be provided");
			}
		}
	})
})

app.listen(port,() => console.log(`Server running at port ${port}`))
