const express = require("express");

// Mongoose is a package that allows creation of schemas to model our data structures
// Also has access to a number of methods for manipulating our database
const mongoose = require("mongoose");

const app = express();
const port = 3001;

// MongoDB connection
// connect to the database by passing in our connection string, remember to replace the password and database names with actual values
// {newUrlParser:true} allows us to avoid any current and future errors while connecting to MongoDB

// Syntax
    // mongoose.connect("<MongoDB connection string>",{useNewUrlParser:true})

// Connecting to MongoDb Atlas

mongoose.connect("mongodb+srv://admin123:admin123@project0.zzkxx4f.mongodb.net/s35?retryWrites=true&w=majority",
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
);

// Set notification for connection success or failure
// connection to the database
// allows us to handle errors when the initial connection is established
// works with the on and once Mongoose methods
let db = mongoose.connection;

db.on("error", console.error.bind(console,"connection error"));
// if a connection error occured, output in the console
// console.error.bind(console) allows us to print errors in our terminal

db.once("open", () => console.log ("We're connected to the cloud database"))

// Mongoose Schemas
// Schemas determine the structure of the documents to be written in the database
// Schemas act as blueprints to our data
// use the Schema() constructor of the Mongoose module to create a new Schema object
// the "new" keyword creates a new Schema

const taskSchema = new mongoose.Schema({
    // define the fields with their corresponding data type

    name: String,
    status: {
        type: String,
        default: "pending"
    }
})

// Models
// uses Schemas and are used to create/instantiate objects that correspond to the Schema
// Server>Schema(blueprint)>Database>Collection

const Task = mongoose.model("Task", taskSchema)

// Setup for allowing the server to handle data from requests
app.use(express.json());
app.use(express.urlencoded({extended:true}))

// Create a new task
// Business Logic
/* 
    1. Add a functionality to check if there are duplicate tasks
        - if the task already exists in the database, we return an error
        - if the task does not exist in the database, we add it in the database
    2. The task data will be coming from the request's body
    3. Create a new Task object with a "name" field/property
    4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/

app.post("/tasks",(req,res) => {
    Task.findOne({name:req.body.name},(err,result) => {
        if (result != null && result.name == req.body.name) {
            return res.send("Duplicate task found")
        } else {
            let newTask = new Task ({
                name: req.body.name
            });
            newTask.save((saveErr, savedTask) => {
                if (saveErr) {
                    return console.error(saveErr);
                } else {
                    return res.status(201).send("New task created");
                }
            })
        }
    })
})


// Getting all the tasks
// Business Logic
/* 
    1. Retrieve all the documents
    2. If an error is encountered, print the error
    3. If no errors are found, send a success status back to the client/Postman and return an array of documents
*/

app.get("/tasks", (req,res) => {
    Task.find({},(err,result) => {
        if (err) {
            return console.log(err);
        } else {
            return res.status(200).json({
                data : result
            })
        }
    })
})

// Activity

// 1. Create a user schema.
const userSchema = new mongoose.Schema({
    userName: String,
    password: String
});

// 2. Create a user model.
const User = mongoose.model("User", userSchema)

// 3. Create a POST route that will access the /signup route that will create a user.


// Registering a user
// Business Logic
/*  1. Add a functionality to check if there are duplicate tasks
        - If the user already exists in the database, we return an error
        - If the user doesn't exist in the database, we add it in the database
    2. The user data will be ccoming from the request's body
    3. Create a new User object with a "username" and "password" fields/properties
*/
app.post("/signup",(req,res) => {
    User.findOne({userName: req.body.userName}, (err,result) => {
        if (result != null && result.userName == req.body.userName) {
            return res.send("User already exist");
        } else {
            if (req.body.userName != "" && req.body.password !== "") {

                let newUser = new User ({
                    userName: req.body.userName,
                    password: req.body.password
                });

                newUser.save((saveErr, savedUser) => {
                    if (saveErr) {
                        return console.error(saveErr);
                    } else {
                        return res.status(201).send("New user registered");
                    }
                });

            } else {

                return res.send("Both username and password must be provided.");
                
            }
        }
    })
});

/*
    Stretch Goal:
    Create a route that will retrieve all users.
*/

app.get("/registeredUsers", (req,res) => {
    User.find({},(err,result) => {
        if (err) {
            return console.log(err);        
        } else {
            return res.status(200).json({
                data : result
            })
        }
    })
});




app.listen(port,() => console.log(`Server running at port ${port}`));